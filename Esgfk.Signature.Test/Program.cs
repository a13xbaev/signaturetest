﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using Parma.Esgfk.ApplicationServices;

namespace Esgfk.Signature.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            X509Certificate2 sert = null;
            string sertFileArg = args.FirstOrDefault();
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            string sertFile = !string.IsNullOrEmpty(sertFileArg) 
                ? baseDir + sertFileArg 
                : baseDir + AppSettingsHelper.DefaultSertificateName;

            if (string.IsNullOrEmpty(sertFile))
                throw new ArgumentNullException("sertFile");

            Console.Write("Loading a sertificate...");
            try
            {
                sert = new X509Certificate2(sertFile);
                PrintColorMessage("OK", ConsoleColor.Green);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Loadind sertificate ERROR:");
                PrintError(ex);
                return;
            }

            Console.WriteLine("==Sertificate info==");
            CryptoHelper.PrintSertificate(sert);
            Console.WriteLine("====================");

            Console.Write("Validate sertificate...");
            try
            {
                CryptoHelper.ValidateSertificate(sert);
                PrintColorMessage("OK", ConsoleColor.Green);
            }
            catch (Exception ex)
            {
                PrintError(ex);
            }

            if (sert.HasPrivateKey)
            {
                Console.Write("Try to perform signature...");
                try
                {
                    var signature = CryptoHelper.SignData(sert, Encoding.ASCII.GetBytes("test123"));

                    if (signature != null)
                    {
                        Console.WriteLine("Signature={0}", Encoding.ASCII.GetString(signature));
                        PrintColorMessage("OK", ConsoleColor.Green);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Signature ERROR:");
                    PrintError(ex);
                }
            }
            else
            {
                Console.WriteLine("Sertificate has no PrivateKey");
            }

            Console.ReadLine();

        }

        private static void PrintError(Exception ex)
        {
            if (ex == null)
                throw new ArgumentNullException("Exception");

            PrintColorMessage(ex.Message, ConsoleColor.DarkRed);

            if (ex.InnerException != null)
                PrintColorMessage("Inner exception: " + ex.InnerException.Message, ConsoleColor.DarkRed);

            PrintColorMessage(ex.StackTrace, ConsoleColor.DarkRed);
        }

        private static void PrintColorMessage(string message, ConsoleColor color)
        {
            var clr = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = clr;
        }
    }
}
