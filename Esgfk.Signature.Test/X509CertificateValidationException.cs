﻿using System;
using System.Runtime.Serialization;

namespace Parma.Esgfk.ApplicationServices
{
    [Serializable]
    public class X509CertificateValidationException : InvalidOperationException
    {
        public X509CertificateValidationException()
        {
        }

        public X509CertificateValidationException(string message) : base(message)
        {
        }

        public X509CertificateValidationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected X509CertificateValidationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}