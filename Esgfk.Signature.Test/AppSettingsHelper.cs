﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esgfk.Signature.Test
{
    public static class AppSettingsHelper
    {
        public static T Get<T>(string key, T defaultValue)
        {
            var appSetting = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrWhiteSpace(appSetting))
            {
                return defaultValue;
            }

            var converter = TypeDescriptor.GetConverter(typeof(T));
            var result = (T)converter.ConvertFromInvariantString(appSetting);


            if (result == null)
            {
                throw new KeyNotFoundException(key);
            }

            return result;
        }

        public static bool VerifySignatureOnly => Get("VerifySignatureOnly", false);

        public static bool SkipRevocationCheck => Get("SkipRevocationCheck", false);

        public static string DefaultSertificateName => Get("DefaultSertificateName", "sert.cer");
    }
}
