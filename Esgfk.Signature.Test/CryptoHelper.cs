﻿using Parma.Esgfk.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Esgfk.Signature.Test
{
    public static class CryptoHelper
    {
        public static byte[] SignData(X509Certificate2 certificate, byte[] dataToBeSigned)
        {
            try
            {
                if (!certificate.HasPrivateKey)
                    return null;

                //Create a RSA Provider, using the private key
                RSACryptoServiceProvider rsaCryptoServiceProvider = (RSACryptoServiceProvider)certificate.PrivateKey;

                //Sign the data using a desired hashing algorithm
                return rsaCryptoServiceProvider.SignData(dataToBeSigned, new SHA1CryptoServiceProvider());

            }
            catch
            {
                return null;
            }
        }

        public static void PrintSertificate(X509Certificate2 certificate)
        {
            if (certificate == null)
                throw new ArgumentNullException("serificate");

            Console.WriteLine("Chain is OK={0}", certificate.Verify() ? "true" : "false");
            Console.WriteLine("Archived={0}", certificate.Archived ? "true" : "false");
            Console.WriteLine("FriendlyName={0}", certificate.FriendlyName);
            Console.WriteLine("HasPrivateKey={0}", certificate.HasPrivateKey ? "true" : "false");
            Console.WriteLine("Issuer={0}", certificate.Issuer);
            Console.WriteLine("IssuerName={0}", certificate.IssuerName?.Name);
            Console.WriteLine("NotAfter={0}", certificate.NotAfter);
            Console.WriteLine("NotBefore={0}", certificate.NotBefore);
            Console.WriteLine("PrivateKey={0}", certificate.PrivateKey?.KeyExchangeAlgorithm);
            Console.WriteLine("PublicKey={0}", certificate.PublicKey);
            Console.WriteLine("SerialNumber={0}", certificate.SerialNumber);
            Console.WriteLine("SignatureAlgorithm={0}", certificate.SignatureAlgorithm.FriendlyName);
            Console.WriteLine("Subject={0}", certificate.Subject);
            Console.WriteLine("SubjectName={0}", certificate.SubjectName?.Name);
            Console.WriteLine("Thumbprint={0}", certificate.Thumbprint);
            Console.WriteLine("Version={0}", certificate.Version);

            Console.WriteLine("Extensions:");

            foreach (var item in certificate.Extensions)
            {
                Console.WriteLine(item.Oid.Value);
            }
        }

        public static X509Certificate2 GetSertificateFromSignature(string signature)
        {
            byte[] encodedMessage = Convert.FromBase64String(signature);
            SignedCms signedCms = new SignedCms();

            signedCms.Decode(encodedMessage);
            signedCms.CheckHash();
            signedCms.CheckSignature(AppSettingsHelper.VerifySignatureOnly);
            IEnumerable<X509Certificate2> certificates = signedCms.Certificates.Cast<X509Certificate2>();
            return certificates.LastOrDefault();
        }

        public static void ValidateSertificate(X509Certificate2 certificate)
        {
            X509Chain chain = new X509Chain();
            chain.ChainPolicy.RevocationMode = AppSettingsHelper.SkipRevocationCheck ? X509RevocationMode.NoCheck : X509RevocationMode.Offline;
            chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
            chain.ChainPolicy.VerificationFlags = X509VerificationFlags.NoFlag;

            bool isChainValid = chain.Build(certificate);

            if (!isChainValid)
            {
                Console.WriteLine("Sertificate validation ERROR:");
                Console.WriteLine("ChainStatusCount=" + chain.ChainStatus.Length);

                foreach (var st in chain.ChainStatus)
                {
                    Console.WriteLine("ChainStatus=" + st.Status);
                }

                var status = chain.ChainStatus.FirstOrDefault(x => x.Status != X509ChainStatusFlags.NoError);

                throw new X509CertificateValidationException(RetrieveX509CertificateValidationMessage(status));
            }
        }

        public static X509Certificate2 ValidateRawCertificate(byte[] rawData)
        {
            if (rawData == null)
                throw new ArgumentNullException(nameof(rawData));

            X509Certificate2 certificate;
            try
            {
                certificate = new X509Certificate2(rawData);
            }
            catch (CryptographicException cryptoException)
            {
                throw new X509CertificateValidationException("Переданный файл не является сертификатом", cryptoException);
            }

            ValidateSertificate(certificate);

            return certificate;
        }

        private static string RetrieveX509CertificateValidationMessage(X509ChainStatus status)
        {
            var flags = status.Status;
            StringBuilder result = new StringBuilder();

            //if (flags.HasFlag(X509ChainStatusFlags.NoError))
            //    result.AppendLine("NoError");
            if (flags.HasFlag(X509ChainStatusFlags.NotTimeValid))
                result.AppendLine("Период действия сертификата истек или еще не наступил");
            if (flags.HasFlag(X509ChainStatusFlags.NotTimeNested))
                result.AppendLine("NotTimeNested");
            if (flags.HasFlag(X509ChainStatusFlags.Revoked))
                result.AppendLine("Сертификат был отозван удостоверяющим центром");
            if (flags.HasFlag(X509ChainStatusFlags.NotSignatureValid))
                result.AppendLine("NotSignatureValid");
            if (flags.HasFlag(X509ChainStatusFlags.NotValidForUsage))
                result.AppendLine("NotValidForUsage");
            if (flags.HasFlag(X509ChainStatusFlags.UntrustedRoot))
                result.AppendLine("Удостоверяющий центр, выпустивший сертификат, не является аккредитованным");
            if (flags.HasFlag(X509ChainStatusFlags.RevocationStatusUnknown))
                result.AppendLine("RevocationStatusUnknown");
            if (flags.HasFlag(X509ChainStatusFlags.Cyclic))
                result.AppendLine("Cyclic");
            if (flags.HasFlag(X509ChainStatusFlags.InvalidExtension))
                result.AppendLine("InvalidExtension");
            if (flags.HasFlag(X509ChainStatusFlags.InvalidPolicyConstraints))
                result.AppendLine("InvalidPolicyConstraints");
            if (flags.HasFlag(X509ChainStatusFlags.InvalidBasicConstraints))
                result.AppendLine("InvalidBasicConstraints");
            if (flags.HasFlag(X509ChainStatusFlags.InvalidNameConstraints))
                result.AppendLine("InvalidNameConstraints");
            if (flags.HasFlag(X509ChainStatusFlags.HasNotSupportedNameConstraint))
                result.AppendLine("HasNotSupportedNameConstraint");
            if (flags.HasFlag(X509ChainStatusFlags.HasNotDefinedNameConstraint))
                result.AppendLine("HasNotDefinedNameConstraint");
            if (flags.HasFlag(X509ChainStatusFlags.HasNotPermittedNameConstraint))
                result.AppendLine("HasNotPermittedNameConstraint");
            if (flags.HasFlag(X509ChainStatusFlags.HasExcludedNameConstraint))
                result.AppendLine("HasExcludedNameConstraint");
            if (flags.HasFlag(X509ChainStatusFlags.PartialChain))
                result.AppendLine("PartialChain");
            if (flags.HasFlag(X509ChainStatusFlags.CtlNotTimeValid))
                result.AppendLine("CtlNotTimeValid");
            if (flags.HasFlag(X509ChainStatusFlags.CtlNotSignatureValid))
                result.AppendLine("CtlNotSignatureValid");
            if (flags.HasFlag(X509ChainStatusFlags.CtlNotValidForUsage))
                result.AppendLine("CtlNotValidForUsage");
            if (flags.HasFlag(X509ChainStatusFlags.OfflineRevocation))
                result.AppendLine("OfflineRevocation");
            if (flags.HasFlag(X509ChainStatusFlags.NoIssuanceChainPolicy))
                result.AppendLine("NoIssuanceChainPolicy");
            if (flags.HasFlag(X509ChainStatusFlags.ExplicitDistrust))
                result.AppendLine("ExplicitDistrust");
            if (flags.HasFlag(X509ChainStatusFlags.HasNotSupportedCriticalExtension))
                result.AppendLine("HasNotSupportedCriticalExtension");
            if (flags.HasFlag(X509ChainStatusFlags.HasWeakSignature))
                result.AppendLine("HasWeakSignature");

            return string.Format("status info: {0}, details: {1}", status.StatusInformation, result.ToString());
        }
    }
}
